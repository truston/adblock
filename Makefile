GTEST_DIR = ./lib/test/googletest/googletest
BUILD_DIR = ./build
BIN_DIR = $(BUILD_DIR)/bin
APP_NAME = LiquidDemocracy
TEST_APP_NAME = $(APP_NAME)_UnitTests
APP_TARGET = $(BIN_DIR)/$(APP_NAME)
TEST_TARGET = $(BIN_DIR)/$(TEST_APP_NAME)

CXX = g++
COMMON_CPPFLAGS = -Wall -Wextra -g -std=c++11
COMMON_CPPFLAGS += -isystem ./inc
TEST_CPPFLAGS = -isystem $(GTEST_DIR)/include
APPLIBS = -lstdc++
TESTLIBS = $(APPLIBS) -pthread

APP_OBJS_DIR = $(BUILD_DIR)/$(APP_NAME)/objs
TEST_OBJS_DIR = $(BUILD_DIR)/$(TEST_APP_NAME)/objs

APP_SOURCES = $(wildcard src/*.cc)
APP_OBJECTS = $(patsubst src/%.cc, $(APP_OBJS_DIR)/%.o, $(APP_SOURCES))
HEADERS = $(wildcard inc/*.h)
TEST_SOURCES = $(wildcard test/*.cc)
TEST_OBJECTS = $(patsubst test/%.cc, $(TEST_OBJS_DIR)/%.o, $(TEST_SOURCES))
APP_OBJS_NO_MAIN = $(filter-out $(APP_OBJS_DIR)/main.o, $(APP_OBJECTS))

GTEST_HEADERS = $(GTEST_DIR)/include/gtest/*.h \
                $(GTEST_DIR)/include/gtest/internal/*.h
GTEST_SRCS_ = $(GTEST_DIR)/src/*.cc $(GTEST_DIR)/src/*.h $(GTEST_HEADERS)


$(TEST_OBJS_DIR)/gtest-all.o: $(GTEST_SRCS_) | $(TEST_OBJS_DIR)
	$(CXX) $(COMMON_CPPFLAGS) $(TEST_CPPFLAGS) -I$(GTEST_DIR) $(CXXFLAGS) -c \
            $(GTEST_DIR)/src/gtest-all.cc -o $@

$(TEST_OBJS_DIR)/gtest.a: $(TEST_OBJS_DIR)/gtest-all.o | $(TEST_OBJS_DIR)
	$(AR) $(ARFLAGS) $@ $^


app: $(APP_TARGET)
test: $(TEST_TARGET)
all: app test
.PHONY: all
.DEFAULT_GOAL := all

$(BIN_DIR):
	-mkdir -p $(BIN_DIR)

$(APP_OBJS_DIR):
	-mkdir -p $(APP_OBJS_DIR)

$(TEST_OBJS_DIR):
	-mkdir -p $(TEST_OBJS_DIR)

$(APP_OBJS_DIR)/%.o: src/%.cc $(HEADERS) | $(APP_OBJS_DIR)
	$(CXX) $(COMMON_CPPFLAGS) -c $< -o $@

$(TEST_OBJS_DIR)/%.o: test/%.cc $(HEADERS) | $(TEST_OBJS_DIR)
	$(CXX) $(COMMON_CPPFLAGS) $(TEST_CPPFLAGS) -c $< -o $@

$(APP_TARGET): $(APP_OBJECTS) | $(BIN_DIR)
	$(CXX) $(APP_OBJECTS) -Wall $(APPLIBS) -o $@

$(TEST_TARGET): $(TEST_OBJECTS) $(APP_OBJS_NO_MAIN) $(TEST_OBJS_DIR)/gtest.a | $(BIN_DIR)
	$(CXX) -Wall $(TESTLIBS) $^ -o $@

clean:
	-rm -f $(APP_OBJS_DIR)/*
	-rm -f $(TEST_OBJS_DIR)/*
	-rm -f $(BIN_DIR)/*
