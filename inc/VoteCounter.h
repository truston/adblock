#pragma once

#include <map>
#include <set>
#include <string>
#include <vector>

namespace adblock {

  class VoteCounter {

    public:

      VoteCounter() : mInvalidVotes(0) {};

      virtual ~VoteCounter() {};

      void calculateVotes(const std::vector<std::string>& rawInput);

      void print() const;

      std::multimap<int, std::string> getValidVotes() const;

      int getInvalidVotes() const {
          return mInvalidVotes;
      }

    private:

      const std::string PICK = "pick";
      const std::string DELEGATE = "delegate";
      const char * SEPARATOR = " ";

      // mapping: choice => voters for that choice
      std::map<std::string, std::set<std::string> > mVotesMap;
      int mInvalidVotes;

      // helper methods
      std::map<std::string, std::set<std::string> >::iterator findChoiceForVoter(const std::string& voter);
      // returns mapping: voter => delgeators for that voter
      std::map<std::string, std::set<std::string> > parseAndValidateInput(const std::vector<std::string>& rawInput);
      std::multimap<int, std::string> toMultimap() const;
  };

} // namespace adblock
