#include "VoteCounter.h"

#include <iostream>
#include <string>
#include <vector>

using namespace std;
using namespace adblock;


int main(int argc, char **argv) {
    (void)(argc);
    (void)(argv);
    VoteCounter voteCounter;
    vector<string> strVec;
    string s;

    cout << "Enter input lines in format \"<name> pick|delegate <choice|name>\" and press Ctrl-D when done:" << endl;
    while (getline(cin, s)) {
        strVec.push_back(s);
    }

    voteCounter.calculateVotes(strVec);
    voteCounter.print();
}
