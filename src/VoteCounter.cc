#include "VoteCounter.h"

#include <iostream>
#include <stdio.h>
#include <string.h>

using namespace std;

namespace adblock {

    string trim(const string& str) {
        size_t first = str.find_first_not_of(' ');
        if (string::npos == first) {
            return str;
        }
        size_t last = str.find_last_not_of(' ');
        return str.substr(first, (last - first + 1));
    }

    void VoteCounter::print() const {
        auto printMap = toMultimap();
        cout << "Valid votes:" << endl;
        for (auto it = printMap.rbegin(); it != printMap.rend(); ++it) {
            cout << "\t" << it->first << " " << it->second << endl;
        }
        cout << "Invalid votes:" << endl;
        cout << "\t" <<  mInvalidVotes << endl;
    }

    multimap<int, string> VoteCounter::getValidVotes() const {
        return toMultimap();
    }

    void VoteCounter::calculateVotes(const vector<string>& rawInput) {
        // remove previous voting results
        mVotesMap.clear();
        mInvalidVotes = 0;

        if (rawInput.empty()) {
            return; // early exit: NO-OP
        }
        
        auto delegationsMap = parseAndValidateInput(rawInput);

        // move items from delegationsMap to mVotesMap
        bool delegateFound;
        do {
            delegateFound = false;
            for (auto it = delegationsMap.begin(); it != delegationsMap.end();) {
                auto validOption = findChoiceForVoter(it->first);
                if (validOption != mVotesMap.end()) {
                    validOption->second.insert(it->second.begin(), it->second.end());
                    it = delegationsMap.erase(it);
                    delegateFound = true;
                } else {
                    ++it;
                }
            }
        } while (!delegationsMap.empty() && delegateFound);

        for (auto it = delegationsMap.begin(); it != delegationsMap.end(); ++it) {
            mInvalidVotes += it->second.size();
        }
    }

    map<string, set<string> >::iterator VoteCounter::findChoiceForVoter(const string& voter) {
        auto mapIt = mVotesMap.begin();
        while (mapIt != mVotesMap.end()) {
            auto It = mapIt->second.find(voter);
            if (It != mapIt->second.end()) {
                return mapIt;
            }
            ++mapIt;

        };
        return mapIt;
    }

    map<string, set<string> > VoteCounter::parseAndValidateInput(const vector<string>& rawInput) {
        map<string, set<string> > delegationsMap;
        for (size_t i = 0; i < rawInput.size(); ++i) {
            string words[3];
              char * pch = strtok(const_cast<char*>(rawInput[i].c_str()), SEPARATOR);
              size_t c = 0;
              while (pch != NULL) {
                words[c++] = pch;
                pch = strtok (NULL, SEPARATOR);
            }
            // validate input line
            if ((c < 3) || (words[1] != PICK && words[1] != DELEGATE)) {
                cout << "Skipping incorrect or empty input line:";
                for (size_t j = 0; j < c; ++j) {
                    cout << SEPARATOR << words[j];
                }
                cout << endl;
                continue;
            }

            if (words[1] == PICK) {
                mVotesMap[words[2]].insert(move(words[0]));
            } else {
                delegationsMap[words[2]].insert(move(words[0]));
            }
        }
        return delegationsMap;
    }

    multimap<int, string> VoteCounter::toMultimap() const {
        multimap<int, string> multiMap;
        for (auto it = mVotesMap.begin(); it != mVotesMap.end(); ++it) {
            multiMap.insert(make_pair(it->second.size(), it->first));
        }
        return multiMap;
    }

} // namespace adblock
