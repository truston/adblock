#include "VoteCounter.h"

#include <gtest/gtest.h>
#include <string>

using namespace std;
using namespace adblock;


/* Test against empty input */
TEST(LiquidDemocracyTest, emptyInput) {
    VoteCounter voteCounter;
    vector<string> strVec;
    voteCounter.calculateVotes(strVec);

    EXPECT_EQ(0, voteCounter.getInvalidVotes());
    EXPECT_EQ(0, voteCounter.getValidVotes().size());
}

/* Test against malformed input, that is input is not in the expected format */
TEST(LiquidDemocracyTest, malformedInput) {
    VoteCounter voteCounter;
    vector<string> strVec;
    strVec.push_back("Bob delegateCarol");
    strVec.push_back("Carolpick Salad");
    strVec.push_back("Dave delegates Eve");
    strVec.push_back("Eve picks Mallory");
    strVec.push_back("Dave Delegate Eve");
    strVec.push_back("Eve PICK Mallory");
    strVec.push_back("dummy");
    strVec.push_back("");
    voteCounter.calculateVotes(strVec);

    EXPECT_EQ(0, voteCounter.getInvalidVotes());
    EXPECT_EQ(0, voteCounter.getValidVotes().size());
}

/* Test input with all correct votes, either direct or indirect */
TEST(LiquidDemocracyTest, validInput) {
    VoteCounter voteCounter;
    vector<string> strVec;
    strVec.push_back("Alice pick Pizza");
    strVec.push_back("Bob delegate Carol");
    strVec.push_back("Carol pick Salad");
    strVec.push_back("Dave delegate Eve");
    strVec.push_back("Eve delegate Mallory");
    strVec.push_back("Mallory delegate Alice");
    voteCounter.calculateVotes(strVec);

    EXPECT_EQ(0, voteCounter.getInvalidVotes());
    ASSERT_EQ(2, voteCounter.getValidVotes().size());
    EXPECT_EQ(4, voteCounter.getValidVotes().rbegin()->first);
    EXPECT_STREQ("Pizza", voteCounter.getValidVotes().rbegin()->second.c_str());
    EXPECT_EQ(2, voteCounter.getValidVotes().begin()->first);
    EXPECT_STREQ("Salad", voteCounter.getValidVotes().begin()->second.c_str());
}

/* Test input with all incorrect votes - circular delegation */
TEST(LiquidDemocracyTest, invalidInput) {
    VoteCounter voteCounter;
    vector<string> strVec;
    strVec.push_back("Alice delegate Bob");
    strVec.push_back("Bob delegate Carol");
    strVec.push_back("Carol delegate Dave");
    strVec.push_back("Dave delegate Eve");
    strVec.push_back("Eve delegate Mallory");
    strVec.push_back("Mallory delegate Alice");
    voteCounter.calculateVotes(strVec);

    EXPECT_EQ(6, voteCounter.getInvalidVotes());
    EXPECT_EQ(0, voteCounter.getValidVotes().size());
}

/* Test input with correct and incorrect votes - circular delegation. */
TEST(LiquidDemocracyTest, mixedInput) {
    VoteCounter voteCounter;
    vector<string> strVec;
    strVec.push_back("Alice pick Pizza");
    strVec.push_back("Bob delegate Carol");
    strVec.push_back("Carol pick Salad");
    strVec.push_back("Dave delegate Eve");
    strVec.push_back("Eve delegate Mallory");
    strVec.push_back("Mallory delegate Eve");
    voteCounter.calculateVotes(strVec);

    EXPECT_EQ(3, voteCounter.getInvalidVotes());
    ASSERT_EQ(2, voteCounter.getValidVotes().size());
    EXPECT_EQ(2, voteCounter.getValidVotes().rbegin()->first);
    EXPECT_STREQ("Salad", voteCounter.getValidVotes().rbegin()->second.c_str());
    EXPECT_EQ(1, voteCounter.getValidVotes().begin()->first);
    EXPECT_STREQ("Pizza", voteCounter.getValidVotes().begin()->second.c_str());
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
