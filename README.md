## Checkout
After cloning this repository, please run "git submodule update --init --recursive" to checkout googletest submodule.

## Build
#### Commands
To build this project please run "make" command. There are following build targets:

 * "make" or "make all" - builds app and unit test binaries
 * "make app" - produces app binary
 * "make test" - procused unit test binary
 * "make clean" - removes files from "./build" directory.

#### Outputs
Build output files are paced in "./build" directory.

#### Environment
My local build environment is:

* Target: x86_64-linux-gnu
* gcc version 6.3.0 20170519 (Ubuntu/Linaro 6.3.0-18ubuntu2~16.04)

